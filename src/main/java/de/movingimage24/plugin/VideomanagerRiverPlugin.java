package de.movingimage24.plugin;

import de.movingimage24.river.VmModule;
import org.elasticsearch.plugins.AbstractPlugin;
import org.elasticsearch.river.RiversModule;

/**
 * Created with IntelliJ IDEA.
 * User: joan
 * Date: 28.08.13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
public class VideomanagerRiverPlugin extends AbstractPlugin {

    @Override
    public String name() {
        return "vmriver";
    }

    @Override
    public String description() {
        System.out.println("VideomanagerRiverPlugin: description");
        return "River for vm7";
    }

    public void onModule(RiversModule module){
        System.out.println("VideomanagerRiverPlugin: onmodule");
        module.registerRiver("vmriver", VmModule.class);
    }
}
