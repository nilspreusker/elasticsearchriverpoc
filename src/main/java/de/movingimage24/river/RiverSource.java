package de.movingimage24.river;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.util.GenericType;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: joan
 * Date: 28.08.13
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */
public class RiverSource {

    public List<VideoDto> getAllVideos() throws Exception {
        ClientRequest request = new ClientRequest("http://localhost:" + 8080 + "/mi24-vam/rest/" + "videos");
        request.header("Authorization", "Basic dGVzdEBtb3ZpbmdpbWFnZTI0LmRlOnRlc3Q=");
        ClientResponse<List<VideoDto>> response = request.get(new GenericType<List<VideoDto>>() {
        });
        List<VideoDto> videos = response.getEntity();
        System.out.println("Received " + videos.size() + " videos");
        return videos;
    }

}
