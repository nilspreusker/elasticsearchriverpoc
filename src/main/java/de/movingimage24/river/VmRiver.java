package de.movingimage24.river;

import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.river.AbstractRiverComponent;
import org.elasticsearch.river.River;
import org.elasticsearch.river.RiverName;
import org.elasticsearch.river.RiverSettings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author Joan Raychouni
 */
public class VmRiver extends AbstractRiverComponent implements River {

    @Inject
    protected VmRiver(RiverName riverName, RiverSettings riverSettings, Client client) {
        super(riverName, riverSettings);
        try {
            printSettings(riverSettings);
            RiverSource riverSource = new RiverSource();
            List<VideoDto> allVideos = riverSource.getAllVideos();
            BulkRequestBuilder bulkRequest = client.prepareBulk();
            ObjectMapper mapper = new ObjectMapper();
            for (int i = 0; i < allVideos.size(); i++) {
                VideoDto videoDto = allVideos.get(i);
                byte[] dtoAsBytes = mapper.writeValueAsBytes(videoDto);
                IndexRequest indexRequest = new IndexRequest("myindex", "videos").contentType(XContentType.JSON).id("" + i).source(dtoAsBytes);
                bulkRequest.add(indexRequest);
                logger.info("created:" + indexRequest.toString());
            }

            BulkResponse bulkResponse = bulkRequest.execute().actionGet();
            if (bulkResponse.hasFailures()) {
                logger.debug("error while adding with bulkrequest");
            } else {
                logger.info("Added " + bulkResponse.getItems().length + " items via bulkrequest");
            }
        } catch (Exception e) {
            logger.info("Error in VmRiver : " + e);
        }
    }

    @Override
    public void start() {
        logger.info("VmRiver Starting");

    }

    @Override
    public void close() {
        logger.info("VmRiver closing");

    }

    private void printSettings(RiverSettings riverSettings) {
        Iterator it = riverSettings.settings().keySet().iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            sb.append(riverSettings.settings().get(it.next()));
            if (it.hasNext()) sb.append(",");
        }
        logger.info("Received settings: " + sb.toString());
    }

    private Map createDummyData(int i) {
        Map m = new HashMap();
        m.put("name", "Dave Minion" + i);
        m.put("time", new Random(System.currentTimeMillis()).nextInt());
        return m;
    }
}
