package de.movingimage24.river;

public final class FormatDto {

    private String path;
    private String format;

    private FormatDto() {
    }

    public FormatDto(String path, String format) {
        this.path = path;
        this.format = format;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

}
