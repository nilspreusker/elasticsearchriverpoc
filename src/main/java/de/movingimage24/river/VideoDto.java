package de.movingimage24.river;

import java.util.Collections;
import java.util.List;

/**
 * @author Guy Nouaga Youansi
 */
public class VideoDto {

    private long id;
    private String title;
    private String filename;
    private List<FormatDto> formats;
    private boolean deleted;

    public VideoDto() {
    }

    public VideoDto(String title, String filename) {
        this.title = title;
        this.filename = filename;
        this.formats = Collections.emptyList();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public String getTitle() {
        return title;
    }

    public List<FormatDto> getFormats() {
        return formats;
    }

    public void setFormats(List<FormatDto> formats) {
        this.formats = formats;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
