package de.movingimage24.river;

import org.elasticsearch.common.inject.AbstractModule;
import org.elasticsearch.river.River;

public class VmModule extends AbstractModule {

    @Override
    protected void configure() {
        System.out.println("VmModule: configure");
        bind(River.class).to(VmRiver.class).asEagerSingleton();
    }
}
