Elasticsearch POC-River
==============================

Deployment
--------------
- Build jar-File with

        mvn clean package

- Install the jar as plugin in Elasticsearch
Go into the directory of Elasticsearch and enter the following command:

        ./bin/plugin install river-vm7 -url file:///home/joan/gitProjekte/ElasticsearchPoc/target/mi24-elasticsearch-poc-1.0-SNAPSHOT.jar

- Start Elasticsearch with the command
Go into the directory of elasticsearch and enter the following command:

        ./bin/elasticsearch -f

- Activate the river via curl:

        curl -XPUT localhost:9200/_river/pocvmriver/_meta -d '{"type" : "vmriver"}'

- Deactivate the river via curl:

        curl -XDELETE 'localhost:9200/_river/pocvmriver'